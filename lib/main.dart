import 'package:flutter/material.dart';
//import 'package:splashscreen/splashscreen.dart';
//import 'login_page.dart';
import 'home_page.dart';

void main() => runApp(
    MaterialApp(
      home: Sunsetcast(),
    ));


/*class SunsetcastApp extends StatefulWidget {
  @override
  _SunsetcastAppState createState() => _SunsetcastAppState();
}

class _SunsetcastAppState extends State<SunsetcastApp> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
        seconds: 10,
        navigateAfterSeconds: Sunsetcast(),
        title: Text('welcome to splash',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0
          ),
        ),
        image: Image.asset('sunsetcast_splash_logo.png'),
        backgroundColor: Colors.black,
        styleTextUnderTheLoader: TextStyle(),
        photoSize: 150.0,
        loaderColor: Colors.orange
    );
  }
}*/


class Sunsetcast extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
      home: SunsetcastTabBar(),
    );
  }
}
class SunsetcastTabBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Theme(
          data: ThemeData(
              brightness: Brightness.dark
          ),
          child: Scaffold(
            bottomNavigationBar: TabBar(
                tabs: [
                  Tab(icon: Icon(Icons.home,), text: "Home",),
                  Tab(icon: Icon(Icons.search), text: "Search"),
                  Tab(icon: Icon(Icons.file_download), text: "Account"),
                  Tab(icon: Icon(Icons.list), text: "More"),
                ],
                unselectedLabelColor: Color(0xff999999),
                labelColor: Colors.white,
                indicatorColor: Colors.transparent
            ),
            body: TabBarView(
              children: [
                HomePage(),
                Center( child: Text("Page 2")),
                Center( child: Text("Page 3"),),
                Center( child: Text("Page 4")),
              ],
            ),
          ),
        )
    );
  }
}