//import 'dart:async';
import 'package:flutter/material.dart';
import 'sunsetcastvideoplayer.dart';
//import 'package:video_player/video_player.dart';
class MovieDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.black,
          brightness: Brightness.dark
      ),
      home: HomeView(),
    );
  }
}
class HomeView extends StatelessWidget {

  final TextStyle topMenuStyle = new TextStyle(fontFamily: 'Avenir next',
      fontSize: 20,
      color: Colors.white,
      fontWeight: FontWeight.w600);
  final TextStyle buttonInfoStyle = new TextStyle(fontFamily: 'Avenir next',
      fontSize: 10,
      color: Colors.white,
      fontWeight: FontWeight.w600);

  /* void onPress() {
    print('pressed');
  }*/

  @override
  Widget build(BuildContext context) {
    return new Material(
      child: Container(
        // color: Colors.red,
          child: Center(
            child: ListView(
              children: <Widget>[
                Container(
                  height: 430,
                  // color: Colors.blue,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      /*image: new NetworkImage("https://www.sunsetcast.com/poster/6832388.jpg"),*/
                        image: new NetworkImage(
                            "https://www.sunsetcast.com/poster/10006393.jpg"),
                        fit: BoxFit.fill
                    ),
                  ), // we can change to be backgroundimage instead
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Container(
                                height: 50,
                                width: 100,
                                child: Image(
                                  image: AssetImage(
                                      "assets/just_sunsetcast.png"),
                                )
                              /*child: CachedNetworkImage(
                                    placeholder: (context, url) => CircularProgressIndicator(),
                                    imageUrl: 'https://www.sunsetcast.com/poster/6832388.jpg',
                                )
                                child: Image.network('https://www.sunsetcast.com/poster/6832388.jpg',
                                  repeat: ImageRepeat.noRepeat,
                                  scale: 1.0,


                                )*/ //

                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20, bottom: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[

                      FlatButton(
                        color: Colors.white,
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.play_arrow, color: Colors.black,
                            ),
                            Text("Watch Trailer",
                              style: TextStyle(color: Colors.black),)
                          ],
                        ),
                        onPressed: () {
                          Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => new VideoPlayerApp()),
                          );
                        },
                      ),
                      FlatButton(
                        child: Column(
                          children: <Widget>[
                            Icon(Icons.info, color: Colors.white, size: 30,),
                            Text('Info', style: buttonInfoStyle,)
                          ],
                        ),
                        onPressed: () {

                        },
                      )
                    ],
                  ),
                ),
                //makePopularWidget("Popular on Sunsetcast"),
                makeMovieDetailWidget("Movie Detail"),


              ],
            ),
          )
      ),
    );
  }


  Widget makeMovieDetailWidget(String title) {
    return new Container(
      padding: EdgeInsets.only(left: 5, right: 5),
      height: 220,
      child: Column(
        children: <Widget>[
          Expanded(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(title, style: topMenuStyle),
                ]
            ),
          ),
          Container(
            height: 200,
            child: ListView(
                padding: EdgeInsets.all(3),
                scrollDirection: Axis.horizontal,
                //shrinkWrap: true,
                //children: makeMovieDetailWidget()
            ),
          )
        ],
      ),
    );
  }
}
